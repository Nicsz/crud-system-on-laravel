<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use App\Post;

use App\Image;
use Intervention\Image\Facades\Image as ImageInt;
//use Illuminate\Http\Response;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\Session;

// Пакет UploadImage
use Dan\UploadImage\Exceptions\UploadImageException;

use App\Http\Controllers\Controller;

//use App\File;

use App\Http\Controllers\File;

use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;


class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::all();

        return view('index', compact('posts'));
    }

    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request) 
    {

        $this->validate(request(), [
            'title' => 'required|min:2',
            'intro' => 'required|max:35',
            'body' => 'required',
        ]);

        $post = new Post;
        $post->title = request('title');
        $post->slug = request('slug');
        $post->intro = request('intro');

       
        /*if ($request->hasFile('files')) {
            $images = array();
            $images[] = $request->file('files');

            $pathForOrigName = public_path().'/uploads/original/';
            $destinationPath = public_path() . '/uploads/';

            foreach ($images as $image) {
                $imageOrigName = $image->getClientOriginalName();
                $imageName = uniqid(time()). '.' . $image->getClientOriginalExtension();
                $img = ImageInt::make($image);

                $img->save($pathForOrigName . $imageOrigName);
                $img->resize(100, 100)->save($destinationPath . $imageName);

                $post->img = $imageName;
            }
        }*/

        $post->body = request('body');

        $post->save();

        return redirect('/');
    }

    /*public function image(Request $request) {
        $imageName = $request->file->getClientOriginalName();
        $request->file->move(public_path('uploads'), $imageName);
        return response()->json(['uploaded' => '/uploads/' . $imageName]);
    }*/

    public function edit(Post $post) 
    {
        return view('posts.edit', compact('post'));
    }

    public function update(Post $post, Request $request) {
        $this->validate(request(), [
            'title' => 'required|min:2|max:25',
            'intro' => 'required|max:200',
            'body' => 'required|max:5000',
        ]);

        $post->title = request('title');
        $post->slug = request('slug');
        $post->intro = request('intro');

        /*if ($request->hasFile('files')) {

            $pathForOrigName = public_path().'/uploads/original/';
            $destinationPath = public_path(). '/uploads/';

            foreach ($request->file('files') as $image) {
                $imageOrigName = $image->getClientOriginalName();

                $imageName = uniqid(time()). '.' . $image->getClientOriginalExtension();
                $img = ImageInt::make($image);

                $img->save($pathForOrigName . $imageOrigName);
                $img->resize(100, 100)->save($destinationPath . $imageName);

                $post->img = $imageName;
            }
        } */

        $post->body = request('body');

        $post->update();

        return redirect('/');
    }

    public function destroy(Post $post) 
    {
        $post->delete();
        return redirect('/');
    }
}


