#Laravel 5.8 - Simple CRUD Demo

Simple project showing how create CRUD project on Laravel.

One CRUD operation to manage Companies - create/edit/update/delete.

##How to use

Clone the repository with git clone
Copy .env.example file to .env and edit database credentials there

```
composer install
php artisan key:generate
php artisan migrate
php artisan serve
```
